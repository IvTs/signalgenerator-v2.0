#ifndef WAVE_GENERATOR_H_
#define WAVE_GENERATOR_H_

#define pi180 0.0174

float mass_sin[]={0, 0.5, 0.75, 0.93, 1, 0.93, 0.75, 0.5, 0, -0.5, -0.75, -0.93, -1, -0.93, -0.75, -0.5};
float mass_sin1[]={0, 0.77, 1, 0.77, 0 , -0.77, -1, -0.77};
float mass_sin2[]={0, 1, 0 , -1,};
float mass_sin3[]={0, 0.77, 0.97, 0.45, -0.4 , -0.96, -0.8};
float mass_exp[] = {0.0, 0.3, 0.5, 0.6, 0.7, 0.75, 0.77, 0.8, 0.81, 0.83, 0.85, 0.87, 0.89, 0.90, 0.92, 0.94, 0.96, 0.98, 1.0};
int mass_sin_index = 0;     	// ������ ������� ����� ������ �� �������
volatile int sin_value = 0; 	// �������� ���������� ��� ��� ��������� ������
uint16_t presc_sin = 1;     	// �������� ������� ������
uint16_t ampl = 0;         		// ��������� ������
double time = 0; 				// ������� ���������� �������
double time6 = 0; 				// ������� ���������� ������� 6
double time6_diag = 0; 			// ������� ���������� ������� 6

uint8_t flag_modulation_type = 0;	// ��� ���������
uint8_t flag_polarity = 0; 			// ���� ���������� ��������� 0�00 - �������������, 0�01 - �������������, 0�02 - �����������
uint8_t pulse_freqKHz = 0;			// ������� ��������� ������������� �������
uint16_t pulse_latencyMs = 0;		// ������������ ��������� ������������� �������, ��
uint16_t pulse_5xlatencyMs = 0;		// ������������ ��������� ������������� ������� x 5, ��
unsigned long contraction_latencySec = 0; // ������������ ����������, ��� 

unsigned long timer6_level1 = 0;	// ������ �������, ��� ������� �������� ���������
unsigned long timer6_level2 = 0;
unsigned long timer6_level3 = 0;

unsigned long pause_ms = 0;			// ������������ ����� ����� �����������

float k = 0;						// ����������� ���������� ��������� �������
volatile float ampl_float = 0;


uint8_t flag_pulse_onoff = 0;   // ���� ������� 1 ��� 0;

uint8_t carrier_freqKHz = 2;	// ������� ������� � ����������
uint8_t Iout = 0;				// �������� ���

int mass_wave[1500];				// ������ � ������ ������������� �������
uint16_t mass_size = 0;

float mass_modulation[200];			// ������ �� ���������� ���������
uint8_t mass_modulation_size;
uint8_t mass_modulation_index = 0;  
uint16_t modulation_presc = 0;		// �������� ��� ���������

float mass_envelope[]={0.2, 0.5, 0.75, 0.93, 1, 0.93, 0.75, 0.5, 0.2, 0}; // ������ �������� ��������� �������
uint8_t mass_envelope_index = 0; 	// ������ ������� ���������

int timer3_modulation_counter = 0;

uint8_t flip_flag = 0x00; 			// ���� ����������� ��� 0�0a ���������
uint8_t modulation_cnt = 0;         // ������� ���������� �������� ���������
uint8_t pause_flag = 0;

// ��������������� �����
uint8_t diag_pulse_start_flag = 0; // ���� ������ �������� � ��������������� ������

void create_mass (int freqKHz);		// ������� ������ ������������� �������
void modulation_contol (void);      // ���������� ����������

uint8_t get_random_value(uint8_t min, uint8_t max); // ��������� ��������� ��������


#endif /* UART3_H_ */