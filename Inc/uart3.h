#ifndef UART3_H_
#define UART3_H_

extern UART_HandleTypeDef huart3;
#define PRMBL 2
#define pi180 0.0174

uint8_t rx_buf_US3[SIZE_RX_BUF];    // �������� ����� �� USART3
uint8_t rx_buf_US3_index = 0;       // ������� ������ ��������� ������ USART3 
uint8_t tmp_char[1];                // ���� �������� �������� �� 1 ����� �� USART3, ��������� ������ � 0-�� �������
uint8_t flag_rx_buf_US3_IT = 0;     // ���� 1 - ���� ���������� �� ������ �������

extern uint16_t mass_size;
extern int mass_wave[];
extern uint8_t Iout;
extern uint16_t ampl;
extern uint8_t flag_modulation_type;	// ��� ���������
extern uint8_t flag_polarity; 			// ���� ���������� ��������� 0�00 - �������������, 0�01 - �������������, 0�02 - �����������
extern uint8_t pulse_freqKHz;			// ������� ��������� ������������� �������
extern uint16_t pulse_latencyMs;		// ������������ ��������� ������������� �������, �c
extern uint16_t pulse_5xlatencyMs;		// ������������ ��������� ������������� ������� x 5, �c
extern uint8_t carrier_freqKHz;			// ������� ������� � ����������      
extern unsigned long contraction_latencySec;   	// ������������ ����������, ��� 
extern uint16_t presc_sin;

extern unsigned long timer6_level1;		// ������ �������, ��� ������� �������� ���������
extern unsigned long timer6_level2;
extern unsigned long timer6_level3;

extern unsigned long pause_ms;			// ������������ ����� ����� �����������

extern float k;							// ����������� ���������� ��������� �������
extern volatile float ampl_float;
extern double time6;					// ������� 6�� �������
extern double time6_diag;				// ������� 6�� �������

extern float mass_modulation[];			// ������ �� ���������� ���������
extern uint8_t mass_modulation_size;
extern uint8_t mass_modulation_index;
extern int timer3_modulation_counter;
extern uint16_t modulation_presc;		// �������� ��� ���������

extern float mass_envelope[]; 			// ������ �������� ��������� �������
extern uint8_t mass_envelope_index; 	// ������ ������� ���������
extern uint8_t flip_flag;

extern uint8_t diag_pulse_start_flag;  // ���� ������ �������� � ��������������� ������

extern void create_mass (int freqKHz);		// ������� ������ ������������� �������
void ReadRxBufUS3 (void);			// ����������� ������ ��������� ������ USART3

#endif /* UART3_H_ */