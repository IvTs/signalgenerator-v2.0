#include "wave_generator.h"

//===============================================================================
//---���������� �� ��������------------------------------------------------------
//===============================================================================
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim==&htim3)
  {
    time++;
    timer3_modulation_counter++;

    if(flag_modulation_type == 5) // 2.6 ����������-�������������� ��� ������� ������� (AMF1)
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
        mass_sin_index = 0;
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (mass_wave[mass_sin_index] * mass_modulation[mass_modulation_index]) + 2048); // ���������� �� ������ DAC_Iout
    }
    else if(flag_modulation_type == 7) // 2.7 ��� ������� ������� ��� ���������� ���� (MT)
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
        mass_sin_index = 0;
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (mass_wave[mass_sin_index] * mass_modulation[mass_modulation_index] * mass_envelope[mass_envelope_index]) + 2048); // ���������� �� ������ DAC_Iout
    }
    else if(flag_modulation_type == 8) // 2.8 ��������� �������������, �������������� ��������
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
        mass_sin_index = 0;
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, ((mass_wave[mass_sin_index] * mass_modulation[mass_modulation_index] * mass_envelope[mass_envelope_index]) * k) + 2048); // ���������� �� ������ DAC_Iout
    }
    else if(flag_modulation_type == 9) // 2.11 ��������� ������������ ��������� (��������)
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
        mass_sin_index = 0;
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (mass_wave[mass_sin_index] * mass_modulation[mass_modulation_index]) + 2048); // ���������� �� ������ DAC_Iout
    }

    else if(flag_modulation_type == 0x0a) // 2.12 ������������ ��������� (��������)
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
        mass_sin_index = 0;
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (mass_wave[mass_sin_index] * mass_modulation[mass_modulation_index] * k) + 2048); // ���������� �� ������ DAC_Iout
    }

    else if(flag_modulation_type == 0x0b) // 2.13 ��������� ���������� ��������� (��������)
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
        mass_sin_index = 0;
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (mass_wave[mass_sin_index] * mass_modulation[mass_modulation_index] * k) + 2048); // ���������� �� ������ DAC_Iout
    }
  }

  if(htim == &htim6)
  {
    timer_LED_cnt++;
    time6++;
    time6_diag++;

    if(timer_LED_cnt > LED_FLASH_PERIOD)  // ������� ����������� ������ ������ ����������
    {
      timer_LED_cnt = 0;
      if(flag_MTK_mode) // ���� ������ � ������ ���������� ��������, �� ������� LED 
        HAL_GPIO_TogglePin(PORT_LED, LED_THERAPYMODE);
      else
        HAL_GPIO_WritePin(PORT_LED, LED_THERAPYMODE, GPIO_PIN_SET); // � ������ ����������� ���� �������� LED
    }
  }
}


//===============================================================================
//---������� ������ ������������� �������----------------------------------------
//===============================================================================
void create_mass (int freqKHz)
{
    int tmp = 70 / freqKHz;                 // 10 ����� ������� ����� ��� ��������� 10 ��� ������
    int step = 360 / tmp;                   // ���������� ��� � ��������

    for(int i = 0; i < 360; i = i + step)
    {
          float sin_tmp = cos(i * pi180);
    //  sin_value = 2048 + /*(uint16_t)*/(mass_sin3[i] * ampl);
        mass_wave[mass_size] = 2048 + (sin_tmp * ampl);
        mass_size++;                        // ���������� ����� �������
    }
}


//================================================================================
//---���������� ����������-------------------------------------------------------
//================================================================================
void modulation_contol (void)
{
  if(rx_buf_US3[2 + PRMBL] == 0x00) // ���� ��������� �����������
  {
//    HAL_DAC_SetValue(&hdac, DAC_U, DAC_ALIGN_12B_R, 0);   // ��������� ���������� �� ����������
    HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048);  // ���������� ��� 0 ���
  }

  switch(flag_modulation_type)
  {
    case 0x00:  // ��������� ����������� ����
    {
      if(!flag_polarity) // � ����������� �� ���������� �������
        HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 + ampl); // ���������� ��� ������ ��������� ���
      else
        HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 - ampl); // ���������� ��� ������ ��������� ���
      break;
    }
    case 0x02: // ������������ ��������� ���������
    {
      if(time6 > pulse_latencyMs)
      {
        time6 = 0;
        if(flag_pulse_onoff)  // 0
        {
          flag_pulse_onoff = 0x00;
          HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048); // ���������� ��� 0 ���
        }
        else                  // 1
        {
          flag_pulse_onoff = 0x01;
          if(!flag_polarity) // � ����������� �� ���������� �������
            HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 + ampl); // ���������� ��� ������ ��������� ���
          else
            HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 - ampl); // ���������� ��� ������ ��������� ���
        }
      }
      break;
    }

    case 0x03: // ������������ ������������
    {
      // ������������ 1 � 5 ������ 0
      if(flag_pulse_onoff)
      {
        if(time6 > pulse_5xlatencyMs)
        {
          time6 = 0;
          flag_pulse_onoff = 0x00;
          HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048); // ���������� ��� 0 ���
        }
      }   
        
      else
      {
        if(time6 > pulse_latencyMs)
        {
          time6 = 0;
          flag_pulse_onoff = 0x01;
          if(!flag_polarity) // � ����������� �� ���������� �������
            HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 + ampl); // ���������� ��� ������ ��������� ���
          else
            HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 - ampl); // ���������� ��� ������ ��������� ���
        }

      }
        
      break;
    }

    case 0x04: // ������������ ���
    {
      if(time6 > timer6_level3 + pause_ms)           // ���� ��������� ������������� ���������� �������� �������
        time6 = 0;
      else if(time6 > timer6_level3)                 
        ampl_float = 0;                              // ��������� = 0 ������� �����
      else if(time6 > timer6_level2)
          ampl_float = k * (timer6_level3 - time6);  // 20% ���� ���������
      else if(time6 > timer6_level1)
        ampl_float = 1;                              // 60% ���� ���������
      else if(time6 < timer6_level1)
          ampl_float = time6 * k;                    // 20% �� ����� ������������ ���� ���������

      break;
    }
  }

  if(flag_modulation_type == 6)
  {
    if (time > presc_sin) // ���� ����� ������������ �� ���� ����� ������
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
      {
        mass_sin_index = 0;
      }
//      sin_value = 2048 + (uint16_t)(mass_sin1[mass_sin_index] * ampl);
//      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, sin_value); // ���������� �� ������ DAC_Iout
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, mass_wave[mass_sin_index]); // ���������� �� ������ DAC_Iout
      time = 0;
    }
  }
  // ������������ ���
  else if(flag_modulation_type == 4)
  {
    if (time > presc_sin) // ���� ����� ������������ �� ���� ����� 
    {
      mass_sin_index += 1; 
      if(mass_sin_index > (mass_size - 1)) 
      {
        mass_sin_index = 0;
      }
        HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (mass_wave[mass_sin_index]* ampl_float) + 2048); // ���������� �� ������ DAC_Iout
        time = 0;
      
    }
  }

  else if(flag_modulation_type == 5) //2.6  ����������-�������������� ��� ������� ������� (AMF1)
  {
    if(timer3_modulation_counter > modulation_presc) // 350 - 250 �� // 1500 - 1 ��
    {
      mass_modulation_index++;
      if(mass_modulation_index > mass_modulation_size - 1)
        mass_modulation_index = 0;
      timer3_modulation_counter = 0;
    }
  }

  else if(flag_modulation_type == 7) //2.7  ��� ������� ������� ��� ���������� ���� (MT)
  {
    if(timer3_modulation_counter > modulation_presc)  // ������� �������� �� ������� ���������
    {
      mass_modulation_index++;
      if(mass_modulation_index > mass_modulation_size - 1)
        mass_modulation_index = 0;
      timer3_modulation_counter = 0;
    }

    if(time6 > timer6_level1)           // ��������� ������� ������� ���������
    {
      if(mass_envelope_index < 9)       // ���� �� ����� �� ������� ����� ��������� = 0
      {
        time6 = 0;                      // ���������
        mass_envelope_index++;
      }
      else
      {
        if(time6 > timer6_level1 + pause_ms)    // ���� ����� �� ������� ����� ��������� = 0
        {
          time6 = 0;                            // ���� ����� � �������� ��� �������
          mass_envelope_index = 0;
        }
      }
    }
  }

  else if(flag_modulation_type == 8) //2.8  ��������� �������������, �������������� ��������
  {
    if(timer3_modulation_counter > modulation_presc)  // ������� �������� �� ������� ���������
    {
      mass_modulation_index++;
      if(mass_modulation_index == (mass_modulation_size - 1) / 2) // �������� ��������������� ������� � ����� 0 ���������
        k = -k;
      if(mass_modulation_index > mass_modulation_size - 1)
        mass_modulation_index = 0; 
      timer3_modulation_counter = 0;
    }

    if(time6 > timer6_level1)           // ��������� ������� ������� ���������
    {
      if(mass_envelope_index < 9)       // ���� �� ����� �� ������� ����� ��������� = 0
      {
        time6 = 0;                      // ���������
        mass_envelope_index++;
      }
      else
      {
        if(time6 > timer6_level1 + pause_ms)    // ���� ����� �� ������� ����� ��������� = 0
        {
          time6 = 0;                            // ���� ����� � �������� ��� �������
          mass_envelope_index = 0;
        }
      }
    }
  }

  else if(flag_modulation_type == 9) // 2.11 ��������� ������������ ��������� (��������)
  {
    if(timer3_modulation_counter > modulation_presc) 
    {
      if(mass_modulation_index == (mass_modulation_size - 1) / 2) // ����� ����� ����� ��������� ������������
      {
        if(time6 > timer6_level1)
        {
          time6 = 0;
          mass_modulation_index++;
          timer3_modulation_counter = 0;
        }
      }
      else
      {
        time6 = 0;
        mass_modulation_index++;
        if(mass_modulation_index == mass_modulation_size - 1)
          mass_modulation_index = 0;
        timer3_modulation_counter = 0;
      }
    }
  }

  else if(flag_modulation_type == 0x0a) // 2.12  ������������ ��������� (��������)
  {
    if(flip_flag == 0)
    {
      if(timer3_modulation_counter > modulation_presc)  // ������� �������� �� ������� ���������
      {
        if(mass_modulation_index == (mass_modulation_size - 1) / 2)
        {
          k = -k; mass_modulation_index++; flip_flag = 1;
        } 
        else
        {
          mass_modulation_index++;
          if(mass_modulation_index > mass_modulation_size - 1)
            mass_modulation_index = 0;
            
        }
        timer3_modulation_counter = 0;
        time6 = 0;
      }
    }
    else
    {
      if(timer3_modulation_counter > 3 * modulation_presc)  // ������� �������� �� ������� ���������
      {
        if(mass_modulation_index == (mass_modulation_size - 1) / 2)
        { 
          if(time6 > timer6_level3)
          {
            k = -k;
            mass_modulation_index++;
            flip_flag = 0;
          }
        }
        else
        {
          mass_modulation_index++;
          if(mass_modulation_index > mass_modulation_size - 1)
            mass_modulation_index = 0;
          time6 = 0;  
        } 
        timer3_modulation_counter = 0; 
      }
    }  
  }

  else if(flag_modulation_type == 0x0b) //2.13  ��������� ���������� ��������� (��������))
  {
    if(timer3_modulation_counter > modulation_presc)  // ������� �������� �� ������� ���������
    {
      if(mass_modulation_index == ((mass_modulation_size - 1) / 2))
      {
        mass_modulation_index++;
        modulation_cnt++;
        if((modulation_cnt > get_random_value(5, 12)) && (!flip_flag) && (!pause_flag)) // ������������� ���������
        {
          modulation_cnt = 0;
          flip_flag = 1;
          k = -k;
          pause_flag = 1;
        }
        if((modulation_cnt > get_random_value(3, 7)) && (flip_flag) && (!pause_flag)) // ������������� ���������
        {
          modulation_cnt = 0;
          flip_flag = 0;
          k = -k;
          pause_flag = 1;
        }
        if(pause_flag)  // ����� = ���������� ����������
        {
          k = 0;
          if(modulation_cnt >= timer6_level3)
          {
            mass_modulation_index++;
            modulation_cnt = 0;
            if(flip_flag) k = -1;
            else k = 1;
            pause_flag = 0;
          }
        }
      }
      else
      {
        mass_modulation_index++;
        if(mass_modulation_index > mass_modulation_size - 1)
          mass_modulation_index = 0;  
      }
      timer3_modulation_counter = 0;
    }
  }

  //
  // ��������������� �����
  //
  else if (flag_modulation_type == 0x10) // ������������� �������
  {
    if ((time6 > 300) && (diag_pulse_start_flag))
    {
      time6 = 0;
      u0 = ADC_result[0] * 0.109;
      u1 = ADC_result[1] * 0.0105;

      strLenght = sprintf(strTxUART3,"U = %.1f B\r\nI = %.1f mkA\r\n", u1, u0);
      HAL_UART_Transmit_IT (&huart3, (uint8_t*)strTxUART3, strLenght); // ��������� �������� ���� � ����������
    }
    if((time6_diag > contraction_latencySec) && (diag_pulse_start_flag)) // �������� ������������ ��������
    {
      diag_pulse_start_flag = 0; // ����� �����, ��� ������� ��������� �������
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048);  // ���������� ��� 0 ���
    }
  }

  else if (flag_modulation_type == 0x11) // "����������" �������
  {
    if ((time6 > 300) && (diag_pulse_start_flag))
    {
      time6 = 0;
      u0 = ADC_result[0] * 0.109;
      u1 = ADC_result[1] * 0.0105;

      strLenght = sprintf(strTxUART3,"U = %.1f B\r\nI = %.1f mkA\r\n", u1, u0);
      HAL_UART_Transmit_IT (&huart3, (uint8_t*)strTxUART3, strLenght); // ��������� �������� ���� � ����������
    }

    
    if((time6_diag < contraction_latencySec) && (diag_pulse_start_flag))
    {
      float val = 0.0;
      val = 1.0/ sqrt(contraction_latencySec / time6_diag); // ������ ������� ���� �������
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, (val * ampl) + 2048); // ���������� �� ������ DAC_Iout  
    }

    if((time6_diag > contraction_latencySec) && (diag_pulse_start_flag)) // �������� ������������ ��������
    {
      diag_pulse_start_flag = 0; // ����� �����, ��� ������� ��������� �������
      HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048);  // ���������� ��� 0 ���
    }
  }


}

uint8_t get_random_value(uint8_t min, uint8_t max) // ��������� ��������� ��������
{
  uint8_t magic_value = ADC_result[0] & 0x00ff;
  uint8_t rand_value =  magic_value;
  if(magic_value < min)
  {
    rand_value = min;
  }
  else
  {
    while(rand_value > max)
    {
      rand_value = rand_value >> 1;
    }
  }
  
  if ((rand_value < min) || (rand_value > max))
    rand_value = max - min;
  return rand_value;
}