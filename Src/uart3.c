#include "uart3.h"

//==============================================================================
//----���������� ���������� �� ������ UART--------------------------------------
//==============================================================================
void HAL_UART_RxCpltCallback (UART_HandleTypeDef *huart)
{
  if(huart == &huart3)                      // ���� ������ ���� �� �� USART3
  {
    rx_buf_US3[rx_buf_US3_index] = tmp_char[0];;       // ������������ � ����� ������ �� USART3 �������� ����
    if(rx_buf_US3[rx_buf_US3_index] == '\r')                           // ���� ��� ������ ������ ����� ������
      flag_rx_buf_US3_IT = 1;               // ����-������� ������� �� UART3
    rx_buf_US3_index++;
    //if(rx_buf_US3_index == SIZE_RX_BUF) // �������������� ������ (����� ���������)
    //  rx_buf_US3_index = 0;
    
    HAL_UART_Receive_IT (&huart3, tmp_char, 1);       // ������� ������ ���������� ����
  }
}


//==============================================================================
//----���������� ���������� �� �������� UART------------------------------------
//==============================================================================
void HAL_UART_TxCpltCallback (UART_HandleTypeDef *huart)
{
  __NOP();
}


//==============================================================================
//----������������ ������ ����������� ��������� ������ USART3--------------------
//==============================================================================
void ReadRxBufUS3 (void)           // ����������� ������ ��������� ������ USART3
{
  if(flag_rx_buf_US3_IT)           // ���� ��� ������ �����
  {
    for(uint8_t i = 0; i != rx_buf_US3_index; i++)      // ����� ������ ������ 0A0A0A
      if((rx_buf_US3[i] == 0x0A) && (rx_buf_US3[i+1] == 0x0A) && (rx_buf_US3[i+2] == 0x0A))
      {
        // 1 ���� - ����� ������ 0�00 - ���������������; 0x01 - ���������������
        // ���� ������ ��������������� ����� 
        if(rx_buf_US3[1 + PRMBL] == 0x01)
        {
          flag_MTK_mode = 0x01;
          HAL_GPIO_WritePin(PORT_RELAY, MODE_RELAY, GPIO_PIN_RESET); // ���� ����������� � ����� �������

          // 10� ���� - ����� ��������� ��������� � ������ �������
          // 0�00 - 1 ��������; 0x01 - 2 ��������
          if(rx_buf_US3[10 + PRMBL] == 0x00)
            HAL_GPIO_WritePin(PORT_RELAY, ELEC_SEL_RELAY, GPIO_PIN_RESET);   // 1� ��������
          else
            HAL_GPIO_WritePin(PORT_RELAY, ELEC_SEL_RELAY, GPIO_PIN_SET);     // 2� ��������

          // 2 ���� - �������
          // 0x00 - ���������� ���������
          // 0�01 - ����� ���������
          // 0�02 - ������� �������� ���� � ����������
          switch(rx_buf_US3[2 + PRMBL])
          {
            case 0x00: // 0x00 - ���������� ���������
            {
              HAL_DAC_SetValue(&hdac, DAC_U, DAC_ALIGN_12B_R, 200); // ��������� ���������� �� ����������
              HAL_TIM_Base_Stop_IT(&htim3);                         // ���������� ������ 3
              HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048);  // ���������� ��� 0 ���
              break;
            }
            case 0x01: // 0�01 - ����� ���������
            {
              // 4 ���� - ���������� �� ������ ����������, � (�������� 1 - 36 �) (����������� ���������)
              if((rx_buf_US3[4 + PRMBL] > 0x00) && (rx_buf_US3[4 + PRMBL] < 37)) // ���� �������� �� 0 �� 37
              {
                HAL_DAC_SetValue(&hdac, DAC_U, DAC_ALIGN_12B_R, (rx_buf_US3[4 + PRMBL]) * 102); // ���������� �� ������ DAC_U �� 0 �� 36
              }

              // 5 ���� - ���� ��������� ����, ��� (�������� 0 - 250 ���)
              ampl = rx_buf_US3[5 + PRMBL] * 7; // 6.982 ������ ��������� �������� ������ ((2048 / 3) * (��� � ��� / 100))
              
              // 6 ���� - ���������� ������� 0�00 - �������������, 0�01 - �������������, 0�02 - �����������
              if(rx_buf_US3[6 + PRMBL] < 0x03)
                flag_polarity = rx_buf_US3[6 + PRMBL];
              else
                flag_polarity = 0x00;

              // 7 ���� - ������� ��������� ��
              if(rx_buf_US3[7 + PRMBL] < 26)
                pulse_freqKHz = rx_buf_US3[7 + PRMBL];

              // 8 ���� - ������������ ���������� ����
              if(rx_buf_US3[8 + PRMBL] < 61)
                contraction_latencySec = rx_buf_US3[8 + PRMBL] * 1000;

              // 9 ���� - ������������ ����� ����� ������������ ����
              if(rx_buf_US3[9 + PRMBL] < 50)
                pause_ms = rx_buf_US3[9 + PRMBL] * 1000;

              // 11 ���� - ������� ������� � ���, (�������� 1 - 10 ���)
              if((rx_buf_US3[11 + PRMBL] > 0) && (rx_buf_US3[11 + PRMBL] < 11)) // ���� �������� �� 0 �� 10 (������� ������� ������)
              {
                carrier_freqKHz = rx_buf_US3[11 + PRMBL];
              }

              /*  // 13 ���� - ������� ��������� 0x00 - 100%; 0x01 - 50%
              if(rx_buf_US3[13 + PRMBL] != 0x00)*/

              // 3 ���� - ��� ���������
              flag_modulation_type = rx_buf_US3[3 + PRMBL];
              switch(flag_modulation_type)
              {
                case 0x02: // ������������ ��������� ��������� 2.1
                {
                  pulse_latencyMs = 500 / pulse_freqKHz;
                  break;
                }
                case 0x03: // ������������ ������������ ��������� 2.2
                {
                  pulse_latencyMs = 180 / pulse_freqKHz;
                  pulse_5xlatencyMs = 5 * pulse_latencyMs;
                  break;
                }
                case 0x04: // ������������ ���
                {
                  mass_size = 0;
                  if((flag_polarity == 0x00) || (flag_polarity == 0x01)) // ���� ������������� ��� ���. ����������
                  {
                    for(int i = 0; i < ampl; i = i + 4)
                    {
                        mass_wave[mass_size] = i;
                        mass_size++;                        // ���������� ����� �������
                    }
                    for(int i = ampl; i > 0; i = i - 4)
                    {
                        mass_wave[mass_size] = i;
                        mass_size++;                        // ���������� ����� �������
                    }
                  }
                  else // ����������� ���
                  {
                    for(int i = -ampl; i < ampl; i = i + 8)
                    {
                        mass_wave[mass_size] = i;
                        mass_size++;                        // ���������� ����� �������
                    }
                    for(int i = ampl; i > -ampl; i = i - 8)
                    {
                        mass_wave[mass_size] = i;
                        mass_size++;                        // ���������� ����� �������
                    }
                  }
                  
                  presc_sin = (50 * 1024 / mass_size) / pulse_freqKHz;        // 6 ��������� * 25 ��

                  timer6_level1 = contraction_latencySec * 0.2; // 20% �� ����� ������������ ���������� ��������� ���������
                  timer6_level2 = contraction_latencySec * 0.8; // 60% ���� ���������
                  timer6_level3 = contraction_latencySec;       // 20% ��������� �������

                  k = 1.0 / timer6_level1;                  // ����������� ���������� ��������� �������
                  ampl_float = 0;
                  time6 = 0;

                  HAL_TIM_Base_Start_IT(&htim3);          // ������ ������� 3
                  break;
                }

                case 0x05: // 2.6 ����������-�������������� ��� ������� ������� (AMF1)
                {
                  mass_size = 0;
                  presc_sin = 0;
                  // ��������� ������ ������� �������
                  int tmp = 50 / carrier_freqKHz;         // 10 ����� ������� ����� ��� ��������� 10 ��� ������
                  int step = 360 / tmp;                   // ���������� ��� � ��������

                  if(flag_polarity == 0x02) // ���� �����������
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      mass_wave[mass_size] = (sin_tmp * ampl);
                      mass_size++;                          // ���������� ����� �������
                    }
                  }
                  else // ���� ������������
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = fabs(cos(i * pi180));
                      mass_wave[mass_size] = (sin_tmp * ampl);
                      mass_size++;                          // ���������� ����� �������
                    }
                  }
                  
                  // ��������� ������ ������������ �������
                  step = 10;
                  mass_modulation_size = 0;
                  // 13 ���� - ������� ��������� 0x00 - 100%; 0x01 - 50%
                  if(rx_buf_US3[13 + PRMBL] == 0x00)
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                      //mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                      mass_modulation_size++;                   // ���������� ����� �������
                    }
                  }
                  else
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      // mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                      mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                      mass_modulation_size++;                   // ���������� ����� �������
                    }
                  }
                    
                  timer3_modulation_counter = 0;
                  modulation_presc = 0;

                  modulation_presc = 1500 / rx_buf_US3[12 + PRMBL] ; // ������ �������� ��� �������� �������� ������� �������

                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }

                case 0x06:
                {
                  mass_size = 0;
                  create_mass(carrier_freqKHz);               // ���������� ������ � ������ ������������� �������
                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }

                case 0x07:
                {
                  mass_size = 0;
                  presc_sin = 0;
                  // ��������� ������ ������� �������
                  int tmp = 50 / carrier_freqKHz;         // 10 ����� ������� ����� ��� ��������� 10 ��� ������
                  int step = 360 / tmp;                   // ���������� ��� � ��������

                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_wave[mass_size] = (sin_tmp * ampl);
                    mass_size++;                          // ���������� ����� �������
                  }
                   // ��������� ������ ������������ �������
                  step = 10;
                  mass_modulation_size = 0;
                  // 13 ���� - ������� ��������� 0x00 - 100%; 0x01 - 50%
                  if(rx_buf_US3[13 + PRMBL] == 0x00)
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                      //mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                      mass_modulation_size++;                   // ���������� ����� �������
                    }
                  }
                  else
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      // mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                      mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                      mass_modulation_size++;                   // ���������� ����� �������
                    }
                  }
                    
                  timer3_modulation_counter = 0;
                  modulation_presc = 0;

                  modulation_presc = 1500 / rx_buf_US3[12 + PRMBL] ; // ������ �������� ��� �������� �������� ������� �������

                  timer6_level1 = contraction_latencySec / 9; //������ ������������ ����� ��������� �� ����� ������������ ����������
                  mass_envelope_index = 0;
                  time6 = 0;

                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }

                case 0x08:
                {
                  mass_size = 0;
                  presc_sin = 0;
                  // ��������� ������ ������� �������
                  int tmp = 50 / carrier_freqKHz;         // 10 ����� ������� ����� ��� ��������� 10 ��� ������
                  int step = 360 / tmp;                   // ���������� ��� � ��������

                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = fabs(cos(i * pi180));
                    mass_wave[mass_size] = (sin_tmp * ampl);
                    mass_size++;                          // ���������� ����� �������
                  }
                  // ��������� ������ ������������ �������
                  step = 10;
                  mass_modulation_size = 0;

                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                    mass_modulation_size++;                   // ���������� ����� �������
                  }
   
                  timer3_modulation_counter = 0;
                  modulation_presc = 0;

                  modulation_presc = 1500 / rx_buf_US3[12 + PRMBL] ; // ������ �������� ��� �������� �������� ������� �������

                  timer6_level1 = contraction_latencySec / 9; //������ ������������ ����� ��������� �� ����� ������������ ����������
                  mass_envelope_index = 0;
                  time6 = 0;
                  k = 1.0;

                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }


                case 0x09: //2.11  ��������� ������������ ��������� (��������)
                {
                  mass_size = 0;
                  presc_sin = 0;
                  // ��������� ������ ������� �������
                  int tmp = 50 / carrier_freqKHz;         // 10 ����� ������� ����� ��� ��������� 10 ��� ������
                  int step = 360 / tmp;                   // ���������� ��� � ��������
                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_wave[mass_size] = (sin_tmp * ampl/2) + ampl/2; // ��������� ��������� ��� ������� 0
                    mass_size++;                          // ���������� ����� �������
                  }
                  
                  // ��������� ������ ������������ �������
                  step = 10;
                  mass_modulation_size = 0;
                  // 13 ���� - ������� ��������� 0x00 - 100%; 0x01 - 50%
                  if(rx_buf_US3[13 + PRMBL] == 0x00)
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                      //mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                      mass_modulation_size++;                   // ���������� ����� �������
                    }
                  }
                  else
                  {
                    for(int i = 0; i < 360; i = i + step)
                    {
                      float sin_tmp = cos(i * pi180);
                      // mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                      mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                      mass_modulation_size++;                   // ���������� ����� �������
                    }
                  }
                    
                  timer3_modulation_counter = 0;
                  modulation_presc = 0;

                  modulation_presc = 1500 / rx_buf_US3[12 + PRMBL] ; // ������ �������� ��� �������� �������� ������� �������
                  time6 = 0;
                  timer6_level1 = pause_ms / 1000;            // ����� ����� ������������ � ��
                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }

                case 0x0a: // 2.12  ������������ ��������� (��������)
                {
                  mass_size = 0;
                  presc_sin = 0;
                  // ��������� ������ ������� �������
                  int tmp = 50 / carrier_freqKHz;         // 10 ����� ������� ����� ��� ��������� 10 ��� ������
                  int step = 360 / tmp;                   // ���������� ��� � ��������
                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_wave[mass_size] = (sin_tmp * ampl/2) + ampl/2; // ��������� ��������� ��� ������� 0
                    mass_size++;                          // ���������� ����� �������
                  }
                  
                  // ��������� ������ ������������ �������
                  step = 10;
                  mass_modulation_size = 0;
                
                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                    //mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                    mass_modulation_size++;                   // ���������� ����� �������
                  }
              
                  timer3_modulation_counter = 0;
                  modulation_presc = 0;

                  modulation_presc = (contraction_latencySec / 7) ; // ������ �������� ��� �������� �������� ������� �������
                  time6 = 0;
                  timer6_level3 = pause_ms / 1000;            // ����� ����� ������������ � ��

                  k = 1.0;
                  if(flag_polarity == 0x00) // ���� ������������� ����������
                    flip_flag = 1;
                  else // ���� �������������
                    flip_flag = 0;

                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }

                case 0x0b: // 2.13 ��������� ���������� ��������� (��������)
                {
                  mass_size = 0;
                  presc_sin = 0;
                  // ��������� ������ ������� �������
                  int tmp = 50 / carrier_freqKHz;         // 10 ����� ������� ����� ��� ��������� 10 ��� ������
                  int step = 360 / tmp;                   // ���������� ��� � ��������
                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_wave[mass_size] = (sin_tmp * ampl/2) + ampl/2; // ��������� ��������� ��� ������� 0
                    mass_size++;                          // ���������� ����� �������
                  }
                  
                  // ��������� ������ ������������ �������
                  step = 10;
                  mass_modulation_size = 0;
                
                  for(int i = 0; i < 360; i = i + step)
                  {
                    float sin_tmp = cos(i * pi180);
                    mass_modulation[mass_modulation_size] = (sin_tmp * 0.5) + 0.5; // ��������� 100%
                    //mass_modulation[mass_modulation_size] = fabs(sin_tmp * 0.5) + 0.5; // ��������� 50%
                    mass_modulation_size++;                   // ���������� ����� �������
                  }
              
                  timer3_modulation_counter = 0;
                  modulation_presc = 0;

                  modulation_presc = 1500 / rx_buf_US3[12 + PRMBL] ; // ������ �������� ��� �������� �������� ������� �������
                  time6 = 0;
                  timer6_level3 = pause_ms / 1000;            // ����� ����� ������������ � ��

                  k = 1.0;

                  HAL_TIM_Base_Start_IT(&htim3);              // ������ ������� 3
                  break;
                }


              }
              break;
            }
            case 0x02: // 0x02 - ������� �������� ���� � ����������
            {
              u0 = ADC_result[0] * 0.109;
              u1 = ADC_result[1] * 0.0105;

              strLenght = sprintf(strTxUART3,"U = %.1f B\r\nI = %.1f mkA\r\n", u1, u0);
            //  HAL_UART_Transmit (&huart3, (uint8_t*)strTxUART3, strLenght, 50); // ��������� �������� ���� � ����������
              HAL_UART_Transmit_IT (&huart3, (uint8_t*)strTxUART3, strLenght); // ��������� �������� ���� � ����������
              break;
            }
          } 
        }

        // ������ ��������������� �����
        else if (rx_buf_US3[1 + PRMBL] == 0x00)
        {
          flag_MTK_mode = 0x00;
          HAL_GPIO_WritePin(PORT_RELAY, MODE_RELAY, GPIO_PIN_SET); // ���� ����������� � ��������������� ����� 

          switch(rx_buf_US3[2 + PRMBL]) // ����� ������� ����/ �����
          {
            case 0x00: // 0x00 - ���������� ���������
            {
              HAL_DAC_SetValue(&hdac, DAC_U, DAC_ALIGN_12B_R, 200);   // ��������� ���������� �� ����������
              HAL_TIM_Base_Stop_IT(&htim3);                         // ���������� ������ 3
              HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048);  // ���������� ��� 0 ���
              diag_pulse_start_flag = 0;
              break;
            }
            case 0x01: // 0�01 - ����� ���������
            {
              // 3 ���� - ��� ���������
              flag_modulation_type = rx_buf_US3[3 + PRMBL];

              // 4 ���� - ���������� �� ������ ����������, � (�������� 1 - 36 �) (����������� ���������)
              if((rx_buf_US3[4 + PRMBL] > 0x00) && (rx_buf_US3[4 + PRMBL] < 37)) // ���� �������� �� 0 �� 37
                HAL_DAC_SetValue(&hdac, DAC_U, DAC_ALIGN_12B_R, (rx_buf_US3[4 + PRMBL]) * 102); // ���������� �� ������ DAC_U �� 0 �� 36

              // 5 ���� - ���� ��������� ����, ��� (�������� 0 - 250 ���)
              ampl = rx_buf_US3[5 + PRMBL] * 7; // 6.982 ������ ��������� �������� ������ ((2048 / 3) * (��� � ��� / 100))

              // 8 ���� - ������������ ���������� ����
              if(rx_buf_US3[8 + PRMBL] < 61)
                contraction_latencySec = rx_buf_US3[8 + PRMBL] * 1000;

              switch(flag_modulation_type)
              {
                case 0x10: // ������������� ������� 
                {
                  diag_pulse_start_flag = 1; // ���� ������ �������� � ��������������� ������
                  time6_diag = 0;
                  HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048 + ampl);  // ���������� ��� 0 ���
                  break;
                }

                case 0x11: // ���������� ������� 
                { 
                  diag_pulse_start_flag = 1; // ���� ������ �������� � ��������������� ������
                  time6_diag = 0;
                  HAL_DAC_SetValue(&hdac,DAC_I,DAC_ALIGN_12B_R, 2048);  // ���������� ��� 0 ���
                  break;
                }


              }
              break;
            }
          }
        }

        HAL_GPIO_TogglePin(PORT_LED, LED_DIAGMODE);
        break;
      }
    rx_buf_US3_index = 0;          // ����� ���� ��� ��������� ���� ����� ��������� �������� ����� � ������
    flag_rx_buf_US3_IT = 0;        // ����� ����� ��������� ������
  }
  
}